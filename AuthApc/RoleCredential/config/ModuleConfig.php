<?php

declare(strict_types=1);

namespace Paneric\AuthApc\RoleCredential\config;

use Paneric\ComponentModuleApc\Action\Config\AbstractModuleConfig;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;

class ModuleConfig extends AbstractModuleConfig implements ModuleConfigInterface
{
    private string $moduleName = 'role-credential';

    public function getModuleName(): string
    {
        return $this->moduleName;
    }
}
