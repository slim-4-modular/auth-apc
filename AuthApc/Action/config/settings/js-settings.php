<?php

return [
    'js-settings' => [

        'auth.actions.get' => [
            'tBody' => [
                'id', 'ref', 'created_at', 'updated_at'
            ],
            'textCenteredClass' => [
                'id', 'created_at', 'updated_at'
            ],
            'urlGetById' => '/auth-apc/action/get/',
            'urlEdit' => '/auth-apc/action/edit/',
            'urlRemove' => '/auth-apc/action/remove/',
            'urlAdd' => '/auth-apc/action/add',
        ],

        'auth.actions.get-paginated' => [
            'tBody' => [
                'id', 'ref', 'created_at', 'updated_at'
            ],
            'textCenteredClass' => [
                'id', 'created_at', 'updated_at'
            ],
            'urlGetById' => '/auth-apc/action/get/',
            'urlEdit' => '/auth-apc/action/edit/',
            'urlRemove' => '/auth-apc/action/remove/',
            'urlAdd' => '/auth-apc/action/add',
        ],

        'auth.action.add' => [
            'fields' => [
                [
                    'type' => 'input',
                    'name' => 'ref',
                    'label' => 'ref',
                    'readonly' => '',
                ],
            ],
            'urlCancel' => '/auth-apc/actions/get-paginated',
            'formUrl' => '/auth-apc/action/add',
        ],

        'auth.action.edit' => [
            'fields' => [
                [
                    'type' => 'input',
                    'name' => 'ref',
                    'label' => 'ref',
                    'readonly' => '',
                ],
            ],
            'urlCancel' => '/auth-apc/actions/get-paginated',
            'formUrl' => '/auth-apc/action/edit/',
        ],

        'auth.action.remove' => [
            'fields' => [
                [
                    'type' => 'input',
                    'name' => 'ref',
                    'label' => 'ref',
                    'readonly' => 'readonly',
                ],
            ],
            'urlCancel' => '/auth-apc/actions/get-paginated',
            'formUrl' => '/auth-apc/action/remove/',
        ],
    ],
];
