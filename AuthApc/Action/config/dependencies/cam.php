<?php

declare(strict_types=1);

use Paneric\AuthApc\Action\config\ModuleConfig;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;
use Psr\Container\ContainerInterface;

return array_merge(
    (array) require './../vendor/paneric/component-module-apc/src/config/dependencies/cam.php',
    [
        ModuleConfigInterface::class => static function (ContainerInterface $container): ModuleConfig {
            return new ModuleConfig(
                $container->get('api_base_url')
            );
        },
    ]
);
