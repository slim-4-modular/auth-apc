<?php

declare(strict_types=1);

return [
    'auth.actions.get' => [
        'content_title' => 'Actions - show all',
        'thead' => [
            '#', 'Reference', 'Created at', 'Updated at'
        ],
        'details' => 'Details',
        'edit' => 'Edit',
        'remove' => 'Remove',
        'add' => 'Add action',
    ],

    'auth.actions.get-paginated' => [
        'content_title' => 'Actions - show all (paginated)',
        'thead' => [
            '#', 'Reference', 'Created at', 'Updated at'
        ],
        'details' => 'Details',
        'edit' => 'Edit',
        'remove' => 'Remove',
        'add' => 'Add action',
    ],

    'auth.action.add' => [
        'content_title' => 'Action - add',
        'ref' => 'Reference',
        'cancel' => 'Cancel',
        'exec' => 'Execute',
    ],

    'auth.action.edit' => [
        'content_title' => 'Action - edit',
        'ref' => 'Reference',
        'cancel' => 'Cancel',
        'exec' => 'Execute',
    ],

    'auth.action.remove' => [
        'content_title' => 'Action - remove',
        'ref' => 'Reference',
        'cancel' => 'Cancel',
        'exec' => 'Execute',
    ],
];
