<?php

declare(strict_types=1);

namespace Paneric\AuthApi\AcnCrd\config;

use Paneric\AuthApi\AcnCrd\Model\AcnCrd;
use Paneric\ComponentModuleApi\Action\Config\ModuleConfigGhostTrait;
use Paneric\ComponentModuleApi\Interfaces\ModuleConfigInterface;

class ModuleConfig implements ModuleConfigInterface
{
    use ModuleConfigGhostTrait;

    protected string $table = '';
    protected string $prefix = '';

    protected string $daoClass = AcnCrd::class;

    protected string $query = "
                    SELECT *
                    FROM action_privilege AS acpv
                        LEFT JOIN action AS acn ON acpv.acpv_action_id = acn.acn_id
                        LEFT JOIN privilege_role AS pvrl ON acpv.acpv_privilege_id = pvrl.pvrl_privilege_id
                            LEFT JOIN role_credential AS rlcr ON pvrl.pvrl_role_id = rlcr.rlcr_role_i 
                                LEFT JOIN credential AS crd ON rlcr.rlcr_credential_id = crd.crd_id
                ";
}
