<?php

declare(strict_types=1);

return [
    '/auth-apc' => [
        'action' => './../AuthApc/Action',
        'actions' => './../AuthApc/Action',
        'privilege' => './../AuthApc/Privilege',
        'privileges' => './../AuthApc/Privilege',
        'role' => './../AuthApc/Role',
        'roles' => './../AuthApc/Role',
        'credential' => './../AuthApc/Credential',
        'credentials' => './../AuthApc/Credential',
        'account' => './../AuthApc/Credential',

        'action-privilege' => './../AuthApc/ActionPrivilege',
        'action-privileges' => './../AuthApc/ActionPrivilege',
        'privilege-role' => './../AuthApc/PrivilegeRole',
        'privilege-roles' => './../AuthApc/PrivilegeRole',
        'role-credential' => './../AuthApc/RoleCredential',
        'role-credentials' => './../AuthApc/RoleCredential',
    ],
];
