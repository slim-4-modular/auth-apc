<?php

declare(strict_types=1);

return [
    'api_base_url' => 'http://auth-api-nginx/auth-api',
];
