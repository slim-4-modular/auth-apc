<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\Interfaces\Action;

interface ActivateApcActionInterface
{
    public function __invoke(
        string $proxyPrefix,
        string $id,
        string $hash,
        ?string $local = null
    ): ?array;
}
