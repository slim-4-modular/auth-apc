<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\Interfaces\Action;

interface LogOutApcActionInterface
{
    public function __invoke(
        string $method,
        string $proxyPrefix,
        ?string $local = null
    ): ?array;
}
