<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\Interfaces\Action;

interface RegisterApcActionInterface
{
    public function __invoke(
        string $method,
        string $proxyPrefix,
        ?array $attributes = null,
        ?string $local = null
    ): ?array;
}
