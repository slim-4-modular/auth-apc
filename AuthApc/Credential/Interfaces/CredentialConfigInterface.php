<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\Interfaces;

interface CredentialConfigInterface
{
    public function register(
        string $prefix,
        ?array $attributes,
        ?string $local = null
    ): array;

    public function activate(
        string $proxyPrefix,
        ?string $local = null
    ): array;

    public function logIn(
        string $proxyPrefix,
        ?array $attributes,
        ?string $local = null
    ): array;

    public function logOut(
        string $proxyPrefix,
        ?string $local = null
    ): array;

    public function getApiBaseUrl(): string;

    public function getModuleName(): string;
}
