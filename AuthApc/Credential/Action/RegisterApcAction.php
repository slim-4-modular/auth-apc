<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\Action;

use Paneric\AuthApc\Credential\Interfaces\Action\RegisterApcActionInterface;
use Paneric\AuthApc\Credential\Interfaces\CredentialConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;

class RegisterApcAction implements RegisterApcActionInterface
{
    public function __construct(
        protected HttpClientInterface $httpClient,
        protected CredentialConfigInterface $config
    ) {
    }

    public function __invoke(
        string $method,
        string $proxyPrefix,
        ?array $attributes = null,
        ?string $local = null
    ): ?array {
        if ($method === 'POST') {
            if (!empty($attributes['terms'])) {
                $attributes['terms'] = 1;
            }
            $config = $this->config->register($proxyPrefix, $attributes, $local);

            $data = $this->httpClient->getJsonResponse(
                $config['method'],
                sprintf(
                    '%s/%s%s',
                    $this->config->getApiBaseUrl(),
                    'account',
                    $config['uri_suffix']
                ),
                $config['options']
            );

            if ($data['status'] === 201) {
                $data['status'] = 303;
                $data['redirect_url'] = $config['redirect_url'];
            }

            return $data;
        }

        return ['status' => 200, 'body' => []];
    }
}
