<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\Action;

use Paneric\AuthApc\Credential\Interfaces\Action\LogOutApcActionInterface;
use Paneric\AuthApc\Credential\Interfaces\CredentialConfigInterface;
use Paneric\Interfaces\Session\SessionInterface;

class LogOutApcAction implements LogOutApcActionInterface
{
    public function __construct(
        protected CredentialConfigInterface $config,
        protected SessionInterface $session
    ) {
    }

    public function __invoke(
        string $method,
        string $proxyPrefix,
        ?string $local = null
    ): ?array {
        if ($method === 'POST') {
            $config = $this->config->logOut($proxyPrefix, $local);

            $this->session->unsetData();

            return [
                'status' => 303,
                'redirect_url' => $config['redirect_url'],
                'body' => []
            ];
        }

        return ['status' => 200, 'body' => []];
    }
}
