<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\Action;

use Paneric\AuthApc\Credential\Interfaces\Action\LogInApcActionInterface;
use Paneric\AuthApc\Credential\Interfaces\CredentialConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;
use Paneric\Interfaces\Session\SessionInterface;

class LogInApcAction implements LogInApcActionInterface
{
    public function __construct(
        protected HttpClientInterface $httpClient,
        protected CredentialConfigInterface $config,
        protected SessionInterface $session
    ) {
    }

    public function __invoke(
        string $method,
        string $proxyPrefix,
        array $attributes = null,
        ?string $local = null
    ): ?array {
        if ($method === 'POST') {
            $config = $this->config->logIn($proxyPrefix, $attributes, $local);

            $data = $this->httpClient->getJsonResponse(
                $config['method'],
                sprintf(
                    '%s/%s%s',
                    $this->config->getApiBaseUrl(),
                    'account',
                    $config['uri_suffix']
                ),
                $config['options']
            );

            if ($data['status'] === 200) {
                $this->session->setData($data['body']['api_token'], 'api_token');

                $data['status'] = 303;
                $data['redirect_url'] = $config['redirect_url'];
            }

            return $data;
        }

        return ['status' => 200, 'body' => []];
    }
}
