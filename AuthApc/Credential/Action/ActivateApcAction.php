<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\Action;

use Paneric\AuthApc\Credential\Interfaces\Action\ActivateApcActionInterface;
use Paneric\AuthApc\Credential\Interfaces\CredentialConfigInterface;
use Paneric\Interfaces\HttpClient\HttpClientInterface;

class ActivateApcAction implements ActivateApcActionInterface
{
    public function __construct(
        protected HttpClientInterface $httpClient,
        protected CredentialConfigInterface $config
    ) {
    }

    public function __invoke(
        string $proxyPrefix,
        string $id,
        string $hash,
        ?string $local = null
    ): ?array {
        $config = $this->config->activate($proxyPrefix, $local);

        $data = $this->httpClient->getJsonResponse(
            $config['method'],
            sprintf(
                '%s/%s%s/%s/%s',
                $this->config->getApiBaseUrl(),
                'account',
                $config['uri_suffix'],
                $id,
                $hash
            ),
            $config['options']
        );

        if ($data['status'] === 200) {
            $data['status'] = 303;
            $data['redirect_url'] = $config['redirect_url'];
        }

        return $data;
    }
}
