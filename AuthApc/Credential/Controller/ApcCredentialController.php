<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\Controller;

use Paneric\AuthApc\Credential\Interfaces\Action\ActivateApcActionInterface;
use Paneric\AuthApc\Credential\Interfaces\Action\LogInApcActionInterface;
use Paneric\AuthApc\Credential\Interfaces\Action\LogOutApcActionInterface;
use Paneric\AuthApc\Credential\Interfaces\Action\RegisterApcActionInterface;
use Paneric\ComponentModuleApc\Controller\ApcModuleController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ApcCredentialController extends ApcModuleController
{
    public function register(
        Request $request,
        Response $response,
        RegisterApcActionInterface $action,
    ): Response {
        $data = $action(
            $request->getMethod(),
            $request->getAttribute('proxy_prefix'),
            $request->getParsedBody(),
            $request->getAttribute('local'),
        );

        if ($data['status'] === 303) {
            return ($this->responder)($response, $data, null);
        }

        return ($this->responder)(
            $response,
            $data,
            sprintf('@%s/register.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function activate(
        Request $request,
        Response $response,
        ActivateApcActionInterface $action,
        string $id,
        string $hash
    ): Response {
        $data = $action(
            $request->getAttribute('proxy_prefix'),
            $id,
            $hash,
            $request->getAttribute('local')
        );

        if ($data['status'] === 303) {
            return ($this->responder)($response, $data, null);
        }

        return ($this->responder)(
            $response,
            $data,
            sprintf('@%s/register.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function logIn(
        Request $request,
        Response $response,
        LogInApcActionInterface $action,
    ): Response {
        $data = $action(
            $request->getMethod(),
            $request->getAttribute('proxy_prefix'),
            $request->getParsedBody(),
            $request->getAttribute('local'),
        );

        if ($data['status'] === 303) {
            return ($this->responder)($response, $data, null);
        }

        return ($this->responder)(
            $response,
            $data,
            sprintf('@%s/log-in.html.twig', $request->getAttribute('proxy_scope'))
        );
    }

    public function logOut(
        Request $request,
        Response $response,
        LogOutApcActionInterface $action,
    ): Response {
        $data = $action(
            $request->getMethod(),
            $request->getAttribute('proxy_prefix'),
            $request->getAttribute('local'),
        );

        if ($data['status'] === 303) {
            return ($this->responder)($response, $data, null);
        }

        return ($this->responder)(
            $response,
            $data,
            sprintf('@%s/log-out.html.twig', $request->getAttribute('proxy_scope'))
        );
    }
}
