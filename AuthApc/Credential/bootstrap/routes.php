<?php

declare(strict_types=1);

use Paneric\AuthApc\Credential\Action\ActivateApcAction;
use Paneric\AuthApc\Credential\Action\LogInApcAction;
use Paneric\AuthApc\Credential\Action\LogOutApcAction;
use Paneric\AuthApc\Credential\Action\RegisterApcAction;
use Paneric\AuthApc\Credential\Controller\ApcCredentialController;
use Paneric\Middleware\CSRFMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require './../vendor/paneric/component-module-apc/src/bootstrap/routes.php';

if (isset($slim, $container, $module, $proxyPrefix)) {
    $routePrefix = '/' . $module;
    $routeNamePrefix = $module;

    if (!empty($proxyPrefix)) {
        $routePrefix = $proxyPrefix  . '/' . $module;
        $routeNamePrefix = str_replace('/', '', $proxyPrefix) . '.' . $module;
        $routeNamePrefix = str_replace('-apc', '', $routeNamePrefix);
    }

    try {
        $slim->map(['GET', 'POST'], $routePrefix . '/register', function (Request $request, Response $response) {
            return $this->get(ApcCredentialController::class)->register(
                $request,
                $response,
                $this->get(RegisterApcAction::class)
            );
        })->setName($routeNamePrefix . '.register')
            ->add(CSRFMiddleware::class);

        $slim->get($routePrefix . '/activate/{id}/{hash}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApcCredentialController::class)->activate(
                $request,
                $response,
                $this->get(ActivateApcAction::class),
                $args['id'],
                $args['hash']
            );
        })->setName($routeNamePrefix . '.activate');

        $slim->map(['GET', 'POST'], $routePrefix . '/log-in', function (Request $request, Response $response) {
            return $this->get(ApcCredentialController::class)->logIn(
                $request,
                $response,
                $this->get(LogInApcAction::class)
            );
        })->setName($routeNamePrefix . '.log-in')
            ->add(CSRFMiddleware::class);

        $slim->map(['GET', 'POST'], $routePrefix . '/log-out', function (Request $request, Response $response) {
            return $this->get(ApcCredentialController::class)->logOut(
                $request,
                $response,
                $this->get(LogOutApcAction::class)
            );
        })->setName($routeNamePrefix . '.log-out')
            ->add(CSRFMiddleware::class);
    } catch (Exception $e) {
    }
}
