<?php

return [
    'js-settings' => [

        'auth.credentials.get' => [
            'tBody' => [
                'email', 'gsm', 'created_at', 'updated_at'
            ],
            'textCenteredClass' => [
                'gsm', 'created_at', 'updated_at'
            ],
            'urlGetById' => '/auth-apc/credential/get/',
            'urlEdit' => '/auth-apc/credential/edit/',
            'urlRemove' => '/auth-apc/credential/remove/',
            'urlAdd' => '/auth-apc/credential/add',
        ],

        'auth.credentials.get-paginated' => [
            'tBody' => [
                'email', 'gsm', 'created_at', 'updated_at'
            ],
            'textCenteredClass' => [
                'gsm', 'created_at', 'updated_at'
            ],
            'urlGetById' => '/auth-apc/credential/get/',
            'urlEdit' => '/auth-apc/credential/edit/',
            'urlRemove' => '/auth-apc/credential/remove/',
            'urlAdd' => '/auth-apc/credential/add',
        ],

        'auth.credential.add' => [
            'fields' => [
                [
                    'type' => 'input',
                    'name' => 'email',
                    'label' => 'email',
                    'readonly' => '',
                ],
                [
                    'type' => 'input',
                    'name' => 'gsm',
                    'label' => 'gsm',
                    'readonly' => '',
                ],
                [
                    'type' => 'input',
                    'name' => 'password_hash',
                    'label' => 'password_hash',
                    'readonly' => '',
                ],
            ],
            'urlCancel' => '/auth-apc/credentials/get-paginated',
            'formUrl' => '/auth-apc/credential/add',
        ],

        'auth.credential.edit' => [
            'fields' => [
                [
                    'type' => 'input',
                    'name' => 'email',
                    'label' => 'email',
                    'readonly' => '',
                ],
                [
                    'type' => 'input',
                    'name' => 'gsm',
                    'label' => 'gsm',
                    'readonly' => '',
                ],
                [
                    'type' => 'input',
                    'name' => 'password_hash',
                    'label' => 'password_hash',
                    'readonly' => '',
                ],
            ],
            'urlCancel' => '/auth-apc/credentials/get-paginated',
            'formUrl' => '/auth-apc/credential/edit',
        ],

        'auth.credential.remove' => [
            'fields' => [
                [
                    'type' => 'input',
                    'name' => 'email',
                    'label' => 'email',
                    'readonly' => '',
                ],
            ],
            'urlCancel' => '/auth-apc/credential/get-paginated',
            'formUrl' => '/auth-apc/credential/remove/',
        ],

        'auth.credential.register' => [
            'fields' => [
                [
                    'type' => 'input',
                    'name' => 'email',
                    'label' => 'email',
                    'readonly' => '',
                ],
                [
                    'type' => 'input',
                    'name' => 'gsm',
                    'label' => 'gsm',
                    'readonly' => '',
                ],
                [
                    'type' => 'input',
                    'name' => 'password_hash',
                    'label' => 'password_hash',
                    'readonly' => '',
                ],
                [
                    'type' => 'input',
                    'name' => 'password_repeat',
                    'label' => 'password_repeat',
                    'readonly' => '',
                ],
                [
                    'type' => 'checkbox',
                    'name' => 'terms',
                    'label' => 'terms',
                    'readonly' => '',
                ],
                [
                    'type' => 'input-hidden',
                    'name' => 'audience',
                    'value' => 'auth-apc'
                ],
            ],
            'urlCancel' => '/auth-apc/credentials/get-paginated',
            'formUrl' => '/auth-apc/credential/register',
        ],
    ],
];
