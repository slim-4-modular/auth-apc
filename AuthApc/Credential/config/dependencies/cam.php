<?php

declare(strict_types=1);

use Paneric\AuthApc\Credential\config\ModuleConfig;
use Paneric\AuthApc\Credential\Interfaces\CredentialConfigInterface;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;
use Psr\Container\ContainerInterface;

return array_merge(
    (array) require './../vendor/paneric/component-module-apc/src/config/dependencies/cam.php',
    [
        ModuleConfigInterface::class => static function (ContainerInterface $container): ModuleConfig {
            return new ModuleConfig(
                $container->get('api_base_url')
            );
        },

        CredentialConfigInterface::class => static function (ContainerInterface $container): ModuleConfig {
            return $container->get(ModuleConfigInterface::class);
        },
    ]
);
