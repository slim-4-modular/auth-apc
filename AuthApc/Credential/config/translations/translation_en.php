<?php

declare(strict_types=1);

return [
    'auth.credentials.get' => [
        'content_title' => 'Credentials - show all',
        'thead' => [
            'Email', 'Mobile', 'Created at', 'Updated at'
        ],
        'details' => 'Details',
        'edit' => 'Edit',
        'remove' => 'Remove',
        'add' => 'Add credential',
    ],

    'auth.credentials.get-paginated' => [
        'content_title' => 'Credentials - show all (paginated)',
        'thead' => [
            'Email', 'Mobile', 'Created at', 'Updated at'
        ],
        'details' => 'Details',
        'edit' => 'Edit',
        'remove' => 'Remove',
        'add' => 'Add credential',
    ],

    'auth.credential.add' => [
        'content_title' => 'Credential - add',
        'email' => 'Email',
        'gsm' => 'Mobile',
        'password_hash' => 'Password',
        'cancel' => 'Cancel',
        'exec' => 'Execute',
    ],

    'auth.credential.edit' => [
        'content_title' => 'Credential - edit',
        'email' => 'Email',
        'gsm' => 'Mobile',
        'password_hash' => 'Password',
        'cancel' => 'Cancel',
        'exec' => 'Execute',
    ],

    'auth.credential.remove' => [
        'content_title' => 'Credential - remove',
        'email' => 'Email',
        'cancel' => 'Cancel',
        'exec' => 'Execute',
    ],

    'auth.credential.register' => [
        'content_title' => 'Credentials - register',
        'email' => 'Email',
        'gsm' => 'Mobile',
        'password_hash' => 'Password',
        'password_repeat' => 'Repeat password',
        'terms' => 'I accept terms and conditions',
        'cancel' => 'Cancel',
        'exec' => 'Execute',
    ],
];
