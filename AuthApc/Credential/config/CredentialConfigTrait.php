<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\config;

trait CredentialConfigTrait
{
    public function register(
        string $proxyPrefix,
        ?array $attributes,
        ?string $local = null
    ): array {
        return [
            'redirect_url' => sprintf(
                '%s?local=%s',
                $proxyPrefix,
                $local
            ),
            'method' => 'POST',
            'uri_suffix' => '/register',
            'options' => $this->getOptions(null, null, $local, $attributes),
        ];
    }

    public function activate(
        string $proxyPrefix,
        ?string $local = null
    ): array {
        return [
            'redirect_url' => sprintf(
                '%s/%ss/log-in?local=%s',
                $proxyPrefix,
                $this->getModuleName(),
                $local
            ),
            'method' => 'GET',
            'uri_suffix' => '/activate',
            'options' => $this->getOptions(null, null, $local, null),
        ];
    }

    public function logIn(
        string $proxyPrefix,
        ?array $attributes,
        ?string $local = null
    ): array {
        return [
            'redirect_url' => sprintf(
                '%s?local=%s',
                $proxyPrefix,
                $local
            ),
            'method' => 'POST',
            'uri_suffix' => '/reset-api-token',
            'options' => $this->getOptions(null, null, $local, $attributes),
        ];
    }

    public function logOut(
        string $proxyPrefix,
        ?string $local = null
    ): array {
        return [
            'redirect_url' => sprintf(
                '%s?local=%s',
                $proxyPrefix,
                $local
            )
        ];
    }
}
