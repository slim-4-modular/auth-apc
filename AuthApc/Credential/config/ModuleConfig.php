<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Credential\config;

use Paneric\AuthApc\Credential\Interfaces\CredentialConfigInterface;
use Paneric\ComponentModuleApc\Action\Config\AbstractModuleConfig;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;

class ModuleConfig extends AbstractModuleConfig implements ModuleConfigInterface, CredentialConfigInterface
{
    use CredentialConfigTrait;

    private string $moduleName = 'credential';

    public function getModuleName(): string
    {
        return $this->moduleName;
    }
}
