<?php

declare(strict_types=1);

namespace Paneric\AuthApc\ActionPrivilege\config;

use Paneric\ComponentModuleApc\Action\Config\AbstractModuleConfig;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;

class ModuleConfig extends AbstractModuleConfig implements ModuleConfigInterface
{
    private string $moduleName = 'action-privilege';

    public function getModuleName(): string
    {
        return $this->moduleName;
    }
}
