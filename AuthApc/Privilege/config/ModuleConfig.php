<?php

declare(strict_types=1);

namespace Paneric\AuthApc\Privilege\config;

use Paneric\ComponentModuleApc\Action\Config\AbstractModuleConfig;
use Paneric\ComponentModuleApc\Interfaces\ModuleConfigInterface;

class ModuleConfig extends AbstractModuleConfig implements ModuleConfigInterface
{
    private string $moduleName = 'privilege';

    public function getModuleName(): string
    {
        return $this->moduleName;
    }
}
