<?php

declare(strict_types=1);

return array_merge(
    require './../AuthApc/config/module-map.php',
    ['main' => './../src'],
);
