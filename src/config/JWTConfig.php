<?php

declare(strict_types=1);

namespace App\config;

use Paneric\Interfaces\Config\ConfigInterface;

class JWTConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'encode' => [
                'issuer' => $_ENV['APP_DOMAIN'],
                'expiration' => 12 * 60 * 60,
            ],
            'decode' => [
                'request_attribute_name' => 'jwt',
                'excluded_route_names' => [
                    'auth.account.register',

                    'auth.account.request-activation-reset',
                    'auth.account.activate',

                    'auth.account.request-password-reset',
                    'auth.account.reset-password',

                    'auth.account.log-in',
                    'auth.account.log-out',
                ],
            ],
        ];
    }
}
