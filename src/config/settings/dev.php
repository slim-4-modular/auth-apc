<?php

declare(strict_types=1);

return [
    'root_folder' => './../',

    'base_uri' => '/auth-apc',
    'base_url' => 'http://127.0.0.1:82',

    'main_route_name' => '',

    'twig' => [
        'templates_dirs' => [// Template paths
            '../src/templates' => 'app',
            '../AuthApc/templates' => 'auth',
        ],
        'options' => [// Twig environment options
            'debug' => true,
            'charset' => 'UTF-8',
            'cache' => false, //'/var/www/auth-apc/var/cache'
            'auto_reload' => null,
            'strict_variables' => false,
            'autoescape' => 'html',
            'optimizations' => -1,
        ],
    ],

    'dbal' => [
        'limit' => 10,
        'host' => $_ENV['DB_HOST'],
        'charset' => 'utf8',
        'dbName' => $_ENV['DB_NAME'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PSWD'],
        'options' => [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::MYSQL_ATTR_FOUND_ROWS => true//count rowCounts even if identical values updated
        ],
    ],

    'csrf' => [
        'csrf_key_name' => 'csrf_key',
        'csrf_key_length' => 32,
        'csrf_hash_name' => 'csrf_hash',
        'error_route_name' => 'error.index'
    ],

    'pagination-extension' => [
        'nav_tag_open' => '<nav><ul class="pagination">',
        'first_tag_open' => '<li class="page-item"><a class="page-link" href="',
        'first_tag_middle' => '">',
        'first_tag_close' => '</a></li>',
        'tag_open' => '<li class="page-item"><a class="page-link" href="',
        'tag_middle' => '">',
        'tag_close' => '</a></li>',
        'current_tag_open' => '<li class="page-item"><a class="page-link-current" href="',
        'current_tag_middle' => '">',
        'current_tag_close' => '<span class="sr-only">(current)</span></a></li>',
        'last_tag_open' => '<li class="page-item"><a class="page-link" href="',
        'last_tag_middle' => '">',
        'last_tag_close' => '</a></li>',
        'nav_tag_close' => '</ul></nav>',

        'links_number' => 2,
    ],
];
