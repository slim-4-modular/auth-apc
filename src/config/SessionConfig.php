<?php

declare(strict_types=1);

namespace App\config;

use Paneric\Interfaces\Config\ConfigInterface;

class SessionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'language' => 'en',

            'cookie_lifetime' => 0,         // Expire on close.
            'cookie_access' => '/',         // SessionWrapper cookie readable in all folders.
            'domain' => '',                 // '' for localhost, '.exemple.com' for others
            'secure' => false,              // in case of https true
            'js_denied' => true,            // Make sure the session cookie is not accessible via javascript.

            'hash_function' => 'sha512',    // Hash algorithm to use for the session. (use hash_algos() to get a list of available hashes.)
            'sid_bits_per_character' => 6, // How many bits per character of the hash. The possible values are '4' (0-9, a-f), '5' (0-9, a-v), and '6' (0-9, a-z, A-Z, "-", ",").
            'use_only_cookies' => 1,        // Force the session to only use cookies, not URL variables.
            'gc_maxlifetime' => '86400',      // session max lifetime

            'cookie_name' => 'scm',         // session cookie name

            'encrypt' => true,
            'regenerate' => false,          // Adviced true, but somehow ajax searcher does not work with it.
        ];
    }
}
