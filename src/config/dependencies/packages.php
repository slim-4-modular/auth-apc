<?php

declare(strict_types=1);

use App\config\GuardConfig;
use App\config\SessionConfig;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Paneric\DBAL\DataPreparator;
use Paneric\DBAL\Manager;
use Paneric\DBAL\PDOBuilder;
use Paneric\DBAL\QueryBuilder;
use Paneric\DBAL\SequencePreparator;
use Paneric\Guard\Guard;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Interfaces\Translator\TranslatorInterface;
use Paneric\Logger\ErrorLogger;
use Paneric\Logger\EventLogger;
use Paneric\Logger\HttpClientLogger;
use Paneric\Pagination\PaginationExtension;
use Paneric\Session\Infrastructure\Session;
use Paneric\Session\Infrastructure\SessionPersister;
use Paneric\Session\Infrastructure\SessionRepository;
use Paneric\Twig\Extension\CSRFExtension;
use Paneric\Twig\Extension\JsSettingsExtension;
use Paneric\Twig\Extension\SessionExtension;
use Paneric\Twig\Extension\TranslatorExtension\TranslationExtension;
use Paneric\Twig\Extension\TranslatorExtension\TranslationService;
use Paneric\Twig\Extension\ValidationExtension;
use Paneric\Twig\TwigBuilder;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use RandomLib\Factory as RandomFactory;
use Twig\Environment as Twig;
use Twig\Extension\DebugExtension;

return [
    ErrorLogger::class => static function (
        ContainerInterface $container
    ): LoggerInterface {
        $logger = new ErrorLogger('error_logger');
        $streamHandler = new StreamHandler('./../var/log/error-logger.log', Level::Debug);
        $streamHandler->setFormatter(
            new LineFormatter(null, null, false, true)
        );
        $logger->pushHandler($streamHandler);
        return $logger;
    },

    EventLogger::class => static function (
        ContainerInterface $container
    ): LoggerInterface {
        $logger = new EventLogger('event_logger');
        $streamHandler = new StreamHandler('./../var/log/event-logger.log', Level::Debug);
        $streamHandler->setFormatter(
            new LineFormatter(null, null, false, true)
        );
        $logger->pushHandler($streamHandler);
        return $logger;
    },

    HttpClientLogger::class => static function (
        ContainerInterface $container
    ): HttpClientLogger {
        $logger = new HttpClientLogger('http_client_logger');
        $streamHandler = new StreamHandler('./../var/log/http-client-logger.log', Level::Debug);
        $streamHandler->setFormatter(
            new LineFormatter(null, null, false, true)
        );
        $logger->pushHandler($streamHandler);
        return $logger;
    },

    TranslatorInterface::class => static function (
        ContainerInterface $container
    ): TranslatorInterface {
        return new TranslationService(
            $container->get('local'),
            $container->get('translations')
        );
    },

    Twig::class => static function (
        ContainerInterface $container
    ): Twig {
        $builder = new TwigBuilder();
        $twig = $builder->build($container->get('twig'));

        if ($_ENV['ENV'] === 'dev') {
            $twig->addExtension(new DebugExtension());
        }

        $twig->addExtension(new TranslationExtension(
            $container->get(TranslatorInterface::class)
        ));
        $twig->addExtension(new SessionExtension(
            $container->get(SessionInterface::class),
            $container->get(TranslatorInterface::class)
        ));
        $twig->addExtension(new CSRFExtension(
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $container->get('csrf'),
        ));
        $twig->addExtension(new JsSettingsExtension(
            $container->get('js-settings')
        ));
        $twig->addExtension(new PaginationExtension(
            $container->get('pagination-extension')
        ));
        $twig->addExtension(new ValidationExtension());

        return $twig;
    },

    GuardInterface::class => static function (ContainerInterface $c): Guard {
        $randomFactory = new RandomFactory();
        return new Guard(
            $randomFactory->getMediumStrengthGenerator(),
            $c->get(GuardConfig::class)()
        );
    },

    Manager::class => static function (ContainerInterface $container): Manager {
        return new Manager(
            (new PDOBuilder())->build($container->get('dbal')),
            new QueryBuilder(new SequencePreparator()),
            new DataPreparator()
        );
    },

    SessionInterface::class => static function (
        ContainerInterface $container
    ): SessionInterface {
        return new Session(
            $container->get(SessionConfig::class),
            new SessionRepository($container->get(Manager::class)),
            new SessionPersister($container->get(Manager::class)),
            $container->get(GuardInterface::class)
        );
    },
];
