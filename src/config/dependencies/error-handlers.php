<?php

declare(strict_types=1);

use Paneric\Logger\ErrorLogger;
use Paneric\Slim\Exception\Handler\Apc\HttpBadRequestExceptionApcHandler;
use Paneric\Slim\Exception\Handler\Apc\HttpExceptionApcHandler;
use Paneric\Slim\Exception\Handler\Apc\HttpNotFoundExceptionApcHandler;
use Paneric\Slim\Exception\Handler\Apc\HttpUnauthorizedExceptionApcHandler;
use Psr\Container\ContainerInterface;

return [
    HttpExceptionApcHandler::class => static function (
        ContainerInterface $container
    ): HttpExceptionApcHandler {
        return new HttpExceptionApcHandler(
            $container->get(ErrorLogger::class)
        );
    },

    HttpBadRequestExceptionApcHandler::class => static function (
        ContainerInterface $container
    ): HttpBadRequestExceptionApcHandler {
        return new HttpBadRequestExceptionApcHandler(
            $container->get(ErrorLogger::class)
        );
    },

    HttpNotFoundExceptionApcHandler::class => static function (
        ContainerInterface $container
    ): HttpNotFoundExceptionApcHandler {
        return new HttpNotFoundExceptionApcHandler(
            $container->get(ErrorLogger::class)
        );
    },

    HttpUnauthorizedExceptionApcHandler::class => static function (
        ContainerInterface $container
    ): HttpUnauthorizedExceptionApcHandler {
        return new HttpUnauthorizedExceptionApcHandler(
            $container->get(ErrorLogger::class)
        );
    },
];
