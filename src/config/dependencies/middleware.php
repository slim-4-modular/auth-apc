<?php

declare(strict_types=1);

use App\config\JWTConfig;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\JWT\JWTAuthorizationMiddleware;
use Paneric\JWT\JWTMiddleware;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Middleware\RouteMiddleware;
use Paneric\ModuleEnvironment\LocalMiddleware;
use Psr\Container\ContainerInterface;

return [
    LocalMiddleware::class => static function (ContainerInterface $container): LocalMiddleware {
        return new LocalMiddleware($container);
    },
    RouteMiddleware::class => static function (ContainerInterface $container): RouteMiddleware {
        return new RouteMiddleware($container);
    },
    JWTMiddleware::class => static function (ContainerInterface $container): JWTMiddleware {
        return new JWTMiddleware(
            $container->get(JWTConfig::class),
            $container->get(GuardInterface::class),
        );
    },
    JWTAuthorizationMiddleware::class => static function (ContainerInterface $container): JWTAuthorizationMiddleware {
        return new JWTAuthorizationMiddleware(
            $container->get(JWTConfig::class)
        );
    },
    CSRFMiddleware::class => static function (ContainerInterface $container): CSRFMiddleware {
        return new CSRFMiddleware(
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $container->get('csrf'),
        );
    },
];
