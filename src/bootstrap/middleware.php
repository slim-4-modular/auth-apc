<?php

declare(strict_types=1);

use DI\DependencyException;
use DI\NotFoundException;
use Paneric\JWT\JWTAuthorizationMiddleware;
use Paneric\JWT\JWTMiddleware;
use Paneric\Logger\ErrorLogger;
use Paneric\Middleware\RouteMiddleware;
use Paneric\Middleware\UriMiddleware;
use Paneric\ModuleEnvironment\LocalMiddleware;
use Paneric\Session\SessionMiddleware;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

if (isset($slim, $container)) {
    try {
//        $slim->add(JWTAuthorizationMiddleware::class);
//        $slim->add(JWTMiddleware::class);
        $slim->add(UriMiddleware::class);
        $slim->add(RouteMiddleware::class);//always before UriMiddleware
        $slim->addBodyParsingMiddleware();
        $slim->addRoutingMiddleware();
        $slim->add(SessionMiddleware::class);
        $slim->add(LocalMiddleware::class);//always before UriMiddleware
        $errorMiddleware = $slim->addErrorMiddleware(
            $_ENV['ENV'] === 'dev' || $_ENV['ENV'] === 'test', //false for PROD environment
            true,
            true,
            $container->get(ErrorLogger::class)
        );
        require './../src/bootstrap/error-handlers.php';
    } catch (DependencyException | NotFoundException | NotFoundExceptionInterface | ContainerExceptionInterface $e) {
        echo $e->getMessage();
    }
}
