<?php

declare(strict_types=1);

use DI\DependencyException;
use DI\NotFoundException;
use Paneric\Slim\Exception\Handler\Apc\HttpBadRequestExceptionApcHandler;
use Paneric\Slim\Exception\Handler\Apc\HttpExceptionApcHandler;
use Paneric\Slim\Exception\Handler\Apc\HttpNotFoundExceptionApcHandler;
use Paneric\Slim\Exception\Handler\Apc\HttpUnauthorizedExceptionApcHandler;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Paneric\Slim\Exception\HttpBadRequestException;
use Paneric\Slim\Exception\HttpException;
use Slim\Exception\HttpNotFoundException as SlimHttpNotFoundException;
use Paneric\Slim\Exception\HttpUnauthorizedException;

if (isset($errorMiddleware, $container)) {
    try {
        $errorMiddleware->setErrorHandler(
            HttpException::class,
            $container->get(HttpExceptionApcHandler::class)
        );

        $errorMiddleware->setErrorHandler(
            HttpBadRequestException::class,
            $container->get(HttpBadRequestExceptionApcHandler::class)
        );

        $errorMiddleware->setErrorHandler(
            SlimHttpNotFoundException::class,
            $container->get(HttpNotFoundExceptionApcHandler::class)
        );

        $errorMiddleware->setErrorHandler(
            HttpUnauthorizedException::class,
            $container->get(HttpUnauthorizedExceptionApcHandler::class)
        );
    } catch (
        DependencyException |
        NotFoundException |
        NotFoundExceptionInterface |
        ContainerExceptionInterface $e
    ) {
    }
}
