<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Paneric\ModuleEnvironment\Environment;
use Slim\App;

try {
    $moduleEnvironmentConfig = require './../src/config/module-environment-config.php';
    $moduleMap = require './../src/config/module-map.php';
    $environment = new Environment();
    $environment->set(
        $moduleEnvironmentConfig,
        $moduleMap,
        $_ENV['ENV']
    );
    $proxyPrefix = $environment->getProxyPrefix();
    $module = $environment->getModule();

    $builder = new ContainerBuilder();
    $builder->useAutowiring(true);
    $builder->useAnnotations(false);
    $builder->addDefinitions($environment->getDefinitions());
    $container = $builder->build(); // Throws uncaught exception
    $container->set('local', $environment->getLocal());
    $slim = $container->get(App::class);
    $container->set('proxy_prefix', $proxyPrefix);

    $fileNames = ['middleware.php', 'routes.php'];
    $processFoldersPaths = $environment->getProcessFoldersPaths();
    foreach ($processFoldersPaths as $path) {
        $folderPath = $path . 'bootstrap/';
        foreach ($fileNames as $fileName) {
            $filePath = $folderPath . $fileName;
            if (file_exists($filePath)) {
                require $filePath;
            }
        }
    }

    $slim->run();
} catch (Exception $e) {
    echo 'AUTH-APC BOOTSTRAP: ' . $e->getMessage();
}
