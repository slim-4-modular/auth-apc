import THead from "./THead.js"
import TBody from "./TBody.js"

export default {
    props: {
        p_items: Object,
        p_dict: Object,
        p_settings: Object
    },
    data() {
        return {
            dItems: this.p_items,
            dDict: this.p_dict,
            dSettings: this.p_settings,
        }
    },
    methods: {
        isTextCentered: function(field) {
            if (this.dSettings.textCenteredClass.includes(field)) {
                return true
            }
            return false
        }
    },
    delimiters: ["[[", "]]"],
    components: {
        THead,
        TBody
    },
    template: `
        <div class="tbl-header">
            <table class="autowidth" cellpadding="0" cellspacing="0" border="0">
                <THead :p_dict="dDict" :p_settings="dSettings" />
                <TBody :p_items="dItems" :p_dict="dDict" :p_settings="dSettings" />
            </table>
        </div>

        <div class="list-buttons text-right">
            <a class="btn btn-primary" :href="dSettings.urlAdd" role="button">[[ dDict['add'] ]]</a>
        </div>
    `,
}
