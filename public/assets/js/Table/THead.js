export default {
    props: {
        p_dict: Object,
        p_settings: Object
    },
    data() {
        return {
            dDict: this.p_dict,
            dSettings: this.p_settings,
        }
    },
    delimiters: ["[[", "]]"],
    template: `
        <thead>
            <tr>
                <th v-for='(field, index) in dDict.thead'>[[ field ]]</th>
                <th class="text-center"></th>
                <th class="text-center"></th>
                <th class="text-center"></th>
            </tr>
        </thead>
    `,
}