export default {
    props: {
        p_items: Object,
        p_dict: Object,
        p_settings: Object
    },
    data() {
        return {
            dItems: this.p_items,
            dDict: this.p_dict,
            dSettings: this.p_settings,
        }
    },
    methods: {
        isTextCentered: function(field) {
            if (this.dSettings.textCenteredClass.includes(field)) {
                return true
            }
            return false
        }
    },
    delimiters: ["[[", "]]"],
    template: `
        <tbody>
            <tr v-for='(item, index) in dItems'>
                <td v-for='(field, key, index) in dSettings.tBody' :class="{ 'text-center': isTextCentered(field) }">[[ item[field] ]]</td>
                <td class="text-center"><a  class="td-link" :href="dSettings.urlGetById + item.id">[[ dDict['details'] ]]</a></td>
                <td class="text-center"><a  class="td-link" :href="dSettings.urlEdit    + item.id">[[ dDict['edit'] ]]</a></td>
                <td class="text-center"><a  class="td-link" :href="dSettings.urlRemove  + item.id">[[ dDict['remove'] ]]</a></td>
            </tr>
        </tbody>
    `,
}