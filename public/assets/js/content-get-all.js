import Table from "./Table/Table.js"

const content = Vue.createApp({
    data() {
        return {
            dPagination: window.vPagination,
            dItems: JSON.parse(window.vItems),
            dDict: JSON.parse(window.vDict),
            dSettings: JSON.parse(window.vSettings),
        }
    },
    delimiters: ["[[", "]]"],
    components: {
        Table
    },
    template: `
        <h1>[[ dDict['content_title'] ]]</h1>
        <Table
            :p_items="dItems"
            :p_dict="dDict"
            :p_settings="dSettings"
        />
        <div v-html="dPagination"></div>
    `,
})

content.mount('#content-get-all')