export default {
    props: {
        p_item: Object,
        p_dict: Object,
        p_settings: Object,
        p_readonly: String,
    },
    data() {
        return {
            dItem: this.p_item,
            dDict: this.p_dict,
            dSettings: this.p_settings,
            dReadonly: this.p_readonly,
        }
    },
    delimiters: ["[[", "]]"],
    template: `
        <div class="form-group">
        </div>
    `,
}