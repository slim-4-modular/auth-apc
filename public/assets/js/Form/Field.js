import Input from "./Input.js"
import InputHidden from "./InputHidden.js"
import Select from "./Select.js"
import Checkbox from "./Checkbox.js"

export default {
    props: {
        p_item: Object,
        p_list: Object,
        p_field: Object,
        p_dict: Object,
    },
    data() {
        console.log(this.p_field)
        return {
            dItem: this.p_item,
            dList: this.p_list,
            dField: this.p_field,
            dDict: this.p_dict,
        }
    },
    delimiters: ["[[", "]]"],
    components: {
        Input,
        InputHidden,
        Select,
        Checkbox
    },
    template: `
        <Input 
            v-if="dField.type=='input'"
            :p_name="dField.name"
            :p_label="dDict[dField.label]"
            :p_value="dItem[dField.name]"
            :p_readonly="dItem[dField.readonly]"
        />
        <InputHidden 
            v-if="dField.type=='input-hidden'"
            :p_name="dField.name"
            :p_value="dField.value"
        />
        <Select
            v-if="dField.type=='select'"
            :p_name="dField.name"
            :p_label="dDict[dField.label]"
            :p_value="dItem[dField.name]"
            :p_list="dList[dField.name]"
            :p_readonly="dItem[dField.readonly]"
        />
        <Checkbox
            v-if="dField.type=='checkbox'"
            :p_name="dField.name"
            :p_label="dDict[dField.label]"
            :p_value="dItem[dField.name]"
            :p_readonly="dItem[dField.readonly]"
        />
    `,
}