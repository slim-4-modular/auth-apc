export default {
    props: {
        p_name: String,
        p_value: String,
    },
    data() {
        return {
            dName: this.p_name,
            dValue: this.p_value,
        }
    },
    delimiters: ["[[", "]]"],
    template: `
        <input required hidden
            :name="dName"
            :value="dValue"
        />
    `,
}