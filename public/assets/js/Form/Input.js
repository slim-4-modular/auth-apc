export default {
    props: {
        p_name: String,
        p_label: String,
        p_value: String,
        p_readonly: String,
    },
    data() {
        return {
            dName: this.p_name,
            dLabel: this.p_label,
            dValue: this.p_value,
            dReadonly: this.p_readonly,
        }
    },
    delimiters: ["[[", "]]"],
    template: `
        <div class="form-group">
            <input class="form-group-item form-group-input" type="text" autocomplete="off" required
                :name="dName"
                :value="dValue"
            />
            <label :for="dName" class="form-group-item form-group-label"><span>[[ dLabel ]]</span></label>
        </div>
    `,
}