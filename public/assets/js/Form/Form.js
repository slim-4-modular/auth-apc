import Field from "./Field.js"

export default {
    props: {
        p_item: Object,
        p_list: Object,
        p_csrf: String,
        p_dict: Object,
        p_settings: Object
    },
    data() {
        return {
            dItem: this.p_item,
            dList: this.p_list,
            dCsrf: this.p_csrf,
            dDict: this.p_dict,
            dSettings: this.p_settings,
        }
    },
    methods: {
        setFormUrl: function() {
            if (this.dItem.id) {
                return this.dSettings.formUrl + this.dItem.id
            }
            return this.dSettings.formUrl
        }
    },
    delimiters: ["[[", "]]"],
    components: {
        Field
    },
    template: `
        <form :action="this.setFormUrl()" method="post" novalidate>
            
            <Field
                v-for="field,index in dSettings.fields"
                :p_item="dItem"
                :p_list="dList"
                :p_field="field"
                :p_dict="dDict"
            />

            <div v-html="dCsrf"></div>
            <div class="form-buttons text-right">
                <a class="btn btn-secondary" :href="dSettings.urlCancel" role="button">[[ dDict['cancel'] ]]</a>
                <button type="submit" class="btn btn-primary">[[ dDict['exec'] ]]</button>
            </div>
        </form>
    `,
}