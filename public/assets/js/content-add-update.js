import Form from "./Form/Form.js"

const content = Vue.createApp({
    data() {
        return {
            dItem: JSON.parse(window.vItem),
            dList: JSON.parse(window.vList),
            dCsrf: window.vCsrf,
            dDict: JSON.parse(window.vDict),
            dSettings: JSON.parse(window.vSettings),
        }
    },
    methods: {
        toggleBox() {
        }
    },
    delimiters: ["[[", "]]"],
    components: {
        Form
    },
    template: `
        <h1>[[ dDict['content_title'] ]]</h1>
        <Form
            :p_item="dItem"
            :p_list="dList"
            :p_csrf="dCsrf"
            :p_dict="dDict"
            :p_settings="dSettings"
        />
    `,
})

content.mount('#content-add-update')