```shell
FROM pimcore/pimcore:PHP8.0-apache

RUN usermod -u 1004 www-data \
    && chown -R www-data:www-data /var/www

COPY . /var/www/html

WORKDIR /var/www/html

COPY ./var/config ./app/Update/config

RUN mkdir -p public/var/assets \
  && mkdir -p var/versions \
  && chown -R www-data:www-data var \
  && chown -R www-data:www-data public/var \
  && chown -R www-data:www-data /etc/apache2/ssl

EXPOSE 80/tcp
EXPOSE 443/tcp

USER www-data:www-data

RUN composer install
```