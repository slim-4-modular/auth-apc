https://pl.reactjs.org/docs/add-react-to-a-website.html

```shell
$ npx babel --watch reactjs --out-dir . --presets react-app/prod
```

[JSX](https://pl.reactjs.org/docs/add-react-to-a-website.html)   
[babeljs](https://babeljs.io/docs/en/babel-cli#compile-directories)
```shell
$ npx babel --watch reactjs/auth --out-file public/js/auth/script-auth-compiled.js --presets react-app/prod
```

```json
{
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "build:mobile": "cp src/Mobile.js src/App.js && npm run build && mv build build-mobile",
    "build:desktop": "cp src/Desktop.js src/App.js && npm run build && mv build build-desktop"
  }
}
```
