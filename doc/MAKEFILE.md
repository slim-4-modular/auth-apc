phpcs:
docker run --init --rm -v "$(PWD)":/app -w /app jakzal/phpqa phpcs --standard=PSR12 ./src

phpcbf:
docker run --init --rm -v "$(PWD)":/app -w /app jakzal/phpqa phpcbf --standard=PSR12 ./src

phpstan:
docker run --init --rm -v "$(PWD)":/app -w /app jakzal/phpqa phpstan analyse src