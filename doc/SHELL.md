### SQL querry from command line
```shell
$ cat filename.sql | mysql -u toor -ptoor
```
```shell
cd /etc
cd - #back to previous
cd ~ #back to home
clear # clear terminal
reset # resets terminal
pushd /var # jump into directory
popd 
sudo !! # previous command but with sudo
history # shows all previous commands as a list
!102 # runs a command from history
HISTTIMEFORMAT="%Y-%m-%d %T " # space at the end is important
history # again and we have commands with exact datetime
sudo apt update; sudo apt dist-upgrade # chain one command after another
sudo apt updated && sudo apt dist-upgrade # chain one command after another, but stops in case of error
tail -f /var/log/syslog # show the content of the file in real time
ls -l # list storage of current directory
cat hello.txt # check content of that file
truncate -s 0 hello.txt # delete only content of a file without deleting the file itself
mount | column -t # column shows the output in columns, for any command that produces output not only mount
```
ctrl + l # clear the screen of the terminal, ctrl + r again, and it shows the following commands

ctrl + r # let you find the history of your taped commands

ctrl + u # deletes all in the line

ctrl + a # puts cursor at the beginning of the line

ctrl + i # puts cursor at the end of the line