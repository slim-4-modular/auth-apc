### Installation

*composer.json*
```json
{
  "config": {
    "bin-dir": "bin"
  },
  "autoload": {
    "psr-4": {
      "Paneric\\NNOptimizer\\": "src/",
      "spec\\Paneric\\NNOptimizer\\": "spec/"
    }
  }
}
```

```shell
$ composer require phpspec/phpspec
```

### Configuration

*phpspec.yaml*
```yaml
suites:
  security_suite:
    namespace: Paneric\NNOptimizer
    psr4_prefix: Paneric\NNOptimizer

formatter.name: pretty

code_generation: true

stop_on_failure: true

verbose: false

#extensions:
#  - PhpSpec\Symfony2Extension\Extension

#matchers:
#  - Acme\Matchers\ValidJsonMatcher
#  - Acme\Matchers\PositiveIntegerMatcher
```

### Test creation 
```shell
$ bin/phpspec desc Paneric/NNOptimizer/Optimization/SettingsManager

# class namespace with "/" instead of "\"
```

### Test example
```php
<?php

namespace spec\App\Action\User;

use App\Action\User\CreateAction;
use App\Entity\User;
use App\Form\UserType;
use App\Library\EmailChecker;
use Doctrine\ORM\EntityManagerInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactoryInterface as FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;

class CreateActionSpec extends ObjectBehavior
{
    public function let(
        EntityManagerInterface $manager,
        FormFactory $formFactory,
        RequestStack $requestStack,
        EmailChecker $emailChecker,
        SessionInterface $session
    ): void {
        $this->beConstructedWith($manager, $formFactory, $requestStack, $emailChecker, $session);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(CreateAction::class);
    }

    public function it_is_invokable_1(
        FormFactory $formFactory,
        RequestStack $requestStack,
        Request $request,
        FormInterface $form,
        FormView $formView
    ): void {
        $requestStack->getCurrentRequest()->willReturn(null);

        $formFactory->create(UserType::class, Argument::type(User::class))->willReturn($form);

        $form->handleRequest($request)->shouldNotBeCalled();
        $form->getData()->shouldNotBeCalled();

        $form->createView()->willReturn($formView);

        $this()->shouldBeArray();
        $this()->shouldReturn([
            'form' => $formView
        ]);
    }

    public function it_is_invokable_2(
        FormFactory $formFactory,
        RequestStack $requestStack,
        Request $request,
        FormInterface $form,
        FormView $formView
    ): void {
        $requestStack->getCurrentRequest()->willReturn($request);

        $formFactory->create(UserType::class, Argument::type(User::class))->willReturn($form);

        $request->getMethod()->willReturn('GET');

        $form->handleRequest($request)->shouldNotBeCalled();
        $form->getData()->shouldNotBeCalled();

        $form->createView()->willReturn($formView);

        $this()->shouldBeArray();
        $this()->shouldReturn([
            'form' => $formView
        ]);
    }

    public function it_is_invokable_3(
        FormFactory $formFactory,
        RequestStack $requestStack,
        Request $request,
        FormInterface $form,
        FormView $formView
    ): void {
        $requestStack->getCurrentRequest()->willReturn($request);

        $formFactory->create(UserType::class, Argument::type(User::class))->willReturn($form);

        $request->getMethod()->willReturn('POST');

        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->willReturn(false);
        $form->isValid()->willReturn(true);

        $form->getData()->shouldNotBeCalled();

        $form->createView()->willReturn($formView);

        $this()->shouldBeArray();
        $this()->shouldReturn([
            'form' => $formView
        ]);
    }

    public function it_is_invokable_4(
        FormFactory $formFactory,
        RequestStack $requestStack,
        Request $request,
        FormInterface $form,
        FormView $formView
    ): void {
        $requestStack->getCurrentRequest()->willReturn($request);

        $formFactory->create(UserType::class, Argument::type(User::class))->willReturn($form);

        $request->getMethod()->willReturn('POST');

        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->willReturn(true);
        $form->isValid()->willReturn(false);

        $form->getData()->shouldNotBeCalled();

        $form->createView()->willReturn($formView);

        $this()->shouldBeArray();
        $this()->shouldReturn([
            'form' => $formView
        ]);
    }

    public function it_is_invokable_5(
        FormFactory $formFactory,
        RequestStack $requestStack,
        Request $request,
        FormInterface $form,
        FormView $formView
    ): void {
        $requestStack->getCurrentRequest()->willReturn($request);

        $formFactory->create(UserType::class, Argument::type(User::class))->willReturn($form);

        $request->getMethod()->willReturn('POST');

        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->willReturn(false);
        $form->isValid()->willReturn(false);

        $form->getData()->shouldNotBeCalled();

        $form->createView()->willReturn($formView);

        $this()->shouldBeArray();
        $this()->shouldReturn([
            'form' => $formView
        ]);
    }

    public function it_is_invokable_6(
        EntityManagerInterface $manager,
        FormFactory $formFactory,
        RequestStack $requestStack,
        EmailChecker $emailChecker,
        Request $request,
        FormInterface $form,
        FormView $formView,
        User $user
    ): void {
        $requestStack->getCurrentRequest()->willReturn($request);

        $formFactory->create(UserType::class, Argument::type(User::class))->willReturn($form);

        $request->getMethod()->willReturn('POST');

        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->willReturn(true);
        $form->isValid()->willReturn(true);

        $form->getData()->willReturn($user);

        $user->getEmail()->willReturn('email');

        $emailChecker->isDisposable(Argument::type('string'))->willReturn(true);

        $manager->persist($user)->shouldNotBeCalled();

        $form->createView()->willReturn($formView);

        $this()->shouldBeArray();
        $this()->shouldReturn([
            'form' => $formView
        ]);
    }

    public function it_is_invokable_7(
        EntityManagerInterface $manager,
        FormFactory $formFactory,
        RequestStack $requestStack,
        EmailChecker $emailChecker,
        Session $session,
        FlashBagInterface $flashBag,
        Request $request,
        FormInterface $form,
        User $user
    ): void {
        $requestStack->getCurrentRequest()->willReturn($request);

        $formFactory->create(UserType::class, Argument::type(User::class))->willReturn($form);

        $request->getMethod()->willReturn('POST');

        $form->handleRequest($request)->shouldBeCalled();

        $form->isSubmitted()->willReturn(true);
        $form->isValid()->willReturn(true);

        $form->getData()->willReturn($user);

        $user->getEmail()->willReturn('email');

        $emailChecker->isDisposable(Argument::type('string'))->willReturn(false);

        $manager->persist($user)->shouldBeCalled();
        $manager->flush()->shouldBeCalled();

        $session->getFlashBag()->willReturn($flashBag);
        $flashBag->add('success', 'New user added successfully!')->shouldBeCalled();

        $form->createView()->shouldNotBeCalled();

        $this()->shouldReturn(null);
    }
}
```

### Test run

```shell
$ bin/phpspec run

$ bin/phpspec run spec/Controller

$ bin/phpspec run spec/Controller/UserControllerSpec.php

$ bin/phpspec run spec/Controller/UserControllerSpec.php:56 # only spec defined in line 56
```