##[Commands](https://book.cakephp.org/phinx/0/en/commands.html#commands)

```shell
$ composer require robmorgan/phinx

$ vendor/bin/phinx init --configuration=./src/config/phinx.php

$ vendor/bin/phinx create Action --configuration=./src/config/phinx.php

$ vendor/bin/phinx migrate --environment=dev --configuration=./src/config/phinx.php

$ vendor/bin/phinx migrate --environment=dev --configuration=./src/config/phinx.php --target 20211227221816

$ vendor/bin/phinx migrate -e dev -c ./src/config/phinx.php -t 20110103081132
```

vendor/bin/phinx create CredentialUser --configuration=./src/config/phinx.php