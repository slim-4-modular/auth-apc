```shell
$ bin/console make:migration
```
```shell
$ bin/console doctrine:migrations:generate
$ ./vendor/bin/doctrine-migrations generate
```
```shell
$ bin/console doctrine:migrations:migrate
```
```shell
bin/console doctrine:migrations:migrate 'DoctrineMigrations\Version20180605025653'
```

```shell
$ bin/console migrations:execute --up 'DoctrineMigrations\Version20220729183405
```



```shell
      doctrine:cache:clear-collection-region     
      doctrine:cache:clear-entity-region         
      doctrine:cache:clear-metadata              
      doctrine:cache:clear-query                 
      doctrine:cache:clear-query-region          
      doctrine:cache:clear-result                
      doctrine:database:create                   
      doctrine:database:drop                     
      doctrine:ensure-production-settings        
      doctrine:mapping:convert                   
      doctrine:mapping:import                    
      doctrine:mapping:info                      
      doctrine:migrations:current                
      doctrine:migrations:diff                   
      doctrine:migrations:dump-schema            
      doctrine:migrations:execute                
      doctrine:migrations:generate               
      doctrine:migrations:latest                 
      doctrine:migrations:list                   
      doctrine:migrations:migrate                
      doctrine:migrations:rollup                 
      doctrine:migrations:status                 
      doctrine:migrations:sync-metadata-storage  
      doctrine:migrations:up-to-date             
      doctrine:migrations:version                
      doctrine:query:dql                         
      doctrine:query:sql                         
      doctrine:schema:create                     
      doctrine:schema:drop                       
      doctrine:schema:update                     
      doctrine:schema:validate                   
      make:migration 
```